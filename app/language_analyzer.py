import os
import sys
import logging
import json
import time
from datetime import datetime
import math

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

from app.lib import common
from app.plugins.airzone.airzone_analyzer import Airzone_Analyzer
from app.plugins.weather.weather_analyzer import Weather_Analyzer

import os

KEYWORDS_MAPPING = []

# ----------------------------------------------------------------------
def get_keys_from_value(d, val):
    return [k for k, v in d.items() if v == val]
# ----------------------------------------------------------------------
def on_message(client, userdata, message):
    logger.debug(f"Message reçu: {message.topic} : {message.payload.decode()}")
    try:
        analyze_request(message.payload.decode())
        print("fin normale on message")
    except Exception as e:
        logger.exception(f"Error decoding message: {e} ")
# ----------------------------------------------------------------------
def analyze_request(words:str)-> None:
    """
    Take a string of words, split it, and find the thema of the incoming request searching matching of items
    from KEYWORDS in the passed words list
    When the thema is found, call the right function to process precise analyze and further action
    :param words: a string containing the words sent by voice recognition
    :return: None
    """
    # avant de splitter, on cherche si certaines séquences particulères sont présentes
    words = words.lower()
    request_is_question = False
    question_seq_found = []
    for seq in common.QUESTION_SEQUENCES:
        if seq in words:
            logger.debug(f"Question détectée: {seq}")
            request_is_question = True
            # enlever les éléments de question de la liste
            question_seq_found.append(seq)
    # enlever les éléments de question de la liste
    for w in question_seq_found:
        words = words.replace(w, '')

    words_list = words.split()
    logger.info(f"Analyzing request : {words_list}")


# KEYWORDS_MAPPING = [{"thema": "weather", "matchwords": KEYWORDS_METEO, "score": 0}, {"thema": "ambiance", "matchwords": KEYWORDS_AMBIANCE, "score": 0}]
    thema = None
    analyzer_class = None
    best_score = 0
    logger.debug("Recherche du theme dans les plugins")
    for mapping in KEYWORDS_MAPPING:
        print(mapping["matchwords"])
        current_thema = mapping["thema"]
        logger.debug(f"Recherche de correspondance avec liste {current_thema}")
        intersect = common.lists_intersection(words_list, mapping["matchwords"])
        print(intersect)
        score = len(intersect)
        if score > best_score:
            best_score = score
            thema = mapping["thema"]
            analyzer_class = mapping["class"]

    print(f"best thema : {thema}: {score} matchs")
    # if analyzer_class is not None:
    #     analyzer_class.analyze(request_is_question, words_list)

    # TODO choisir un plugin/classe en fonction du theme
    if thema == "weather":
        weather_analyzer.analyze(request_is_question, words_list)
    elif thema == "airzone":
        airzone_analyzer.analyze(request_is_question, words_list)
    else:
        logger.info("pas compris")
        publish.single("speech", "Je n'ai pas compris, vous pouvez répéter la question ?", hostname=mqtt_host)

    print("fin normale analyze_request")


if __name__ == "__main__":

    # init program
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s — %(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s')
    logger = logging.getLogger(__name__)

    logger.debug("---------------------------------------")
    all_ok = True
    mqtt_host = os.getenv("MQTT_HOST")
    if not mqtt_host:
        logger.critical("MQTT_HOST env var not found")
        all_ok = False
    else:
        logger.debug("MQTT_HOST=%s"%mqtt_host)

    mqtt_agent_topic = os.getenv("TOPIC_AGENT_LANGUAGE")
    if not mqtt_agent_topic:
        logging.critical("TOPIC_AGENT_LANGUAGE env var not found")
        all_ok = False
    else:
        logging.debug(f"Will subscribe to mqtt_agent_topic {mqtt_agent_topic}")

    mqtt_port = int(os.getenv("MQTT_PORT"))
    if not mqtt_port:
        logger.critical("MQTT_PORT env var not found")
        all_ok = False
    else:
        mqtt_port = int(mqtt_port)
        logger.debug("MQTT_PORT=%s"%mqtt_port)

    mqtt_notification_topic = os.getenv("TOPIC_NOTIFICATION")
    if not mqtt_notification_topic:
        logger.critical("TOPIC_NOTIFICATION env var not found")
        all_ok = False
    else:
        logger.debug(f"TOPIC_NOTIFICATION={mqtt_notification_topic}")

    if not all_ok:
        logger.critical("Some missing envar are missing, the program will not start")
        if mqtt_notification_topic and mqtt_host and mqtt_port is not None:
            error_notification = {"channel": "hermes-status",
                                    "msg": {
                                                "agent": "Agent Language Analyzer",
                                                "status": "Error",
                                                "data": "Some envvars missing"
                                            }
                                  }
            publish.single(mqtt_notification_topic, json.dumps(error_notification), hostname=mqtt_host)

        sys.exit(1)

    analyzers = {}
    airzone_analyzer = Airzone_Analyzer()
    analyzers[airzone_analyzer.thema] = airzone_analyzer
    KEYWORDS_MAPPING.append(airzone_analyzer.KEYWORD_MAPPING)

    weather_analyzer = Weather_Analyzer()
    KEYWORDS_MAPPING.append(weather_analyzer.KEYWORD_MAPPING)

    mqtt_client = mqtt.Client()
    mqtt_client.on_message = on_message
    mqtt_client.connect(mqtt_host, mqtt_port, 60)
    mqtt_client.subscribe(mqtt_agent_topic, 1)

    mqtt_client.loop_forever()

