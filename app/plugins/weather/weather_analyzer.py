from ..Abstract_Analyzer import Abstract_Analyzer
from ...lib import common
from datetime import datetime
import paho.mqtt.publish as publish

class Weather_Analyzer(Abstract_Analyzer):
    @property
    def thema(self):
        return "weather"

    def __init__(self):
        print("---- __init__ Airzone_Analyzer")
        super().__init__()

    def initialize(self):
        self.logger.debug("---- initialize Weather_Analyzer")
        self.MATCHWORDS = []
        self.KEYWORDS_MEASURES = []
        self.KEYWORDS_VERBS = ["pleuvoir", "geler"]
        self.KEYWORDS_OBJECTS = ["météo", "température", "pluie", "froid"]
        self.KEYWORDS_TIME = {"aujourd'hui": "current", "demain": "j+1", "apres-demain": "j+2"}
        self.MATCHWORDS.extend([k for k in self.KEYWORDS_TIME.keys()])
        self.MATCHWORDS.extend(self.KEYWORDS_MEASURES)
        self.MATCHWORDS.extend(self.KEYWORDS_VERBS)
        self.MATCHWORDS.extend(self.KEYWORDS_OBJECTS)
        
        self.KEYWORD_MAPPING =  {"thema": self.thema, "class": __class__, "matchwords": self.MATCHWORDS, "verbs": self.KEYWORDS_VERBS,
          "measures": self.KEYWORDS_MEASURES, "objects": self.KEYWORDS_OBJECTS}
        self.logger.debug(self.KEYWORD_MAPPING)
        super().initialize()

    def analyze(self, request_is_question: bool, words_list: list)->None:
        self.logger.info("Analyzing request in thema: meteo")
        topic_weather_agent = "agents/weather"
        command = None
        # on cherche le jour
        if "aujourd'hui" in words_list:
            command = "current"
        elif "demain" in words_list:
            command = "tomorrow"
        elif "après-demain" in words_list:
            command = "j+2"
        else:
            # translated_day = nom du jour en français
            # on cherche si un des mots passés correspond à un nom de jour traduit
            for day_key, translated_day in common.DAYS.items():
                if translated_day in words_list:
                    print(f"on cherche pour {day_key} - {translated_day}")

                    # on cherche la différence de jours entre le jour demandé et aujourd'hui
                    today_day_key = datetime.today().strftime('%A')
                    self.logger.debug(f"on est {today_day_key}")

                    n = 0
                    for a_day_key in common.DAYS.keys():
                        print(f"{a_day_key}")
                        if a_day_key == today_day_key:
                            today_index = n
                        if a_day_key == day_key:
                            day_index = n
                        n = n + 1
                    if day_index < today_index:
                        day_index = day_index + 7
                    diff_days = day_index - today_index
                    self.logger.debug(f"today_index = {today_index} - day_index = {day_index} --- diff = {diff_days}")
                    command = f"j+{diff_days}"
        if command is None:
            command = "current"

        self.logger.info(f"argument: {command}")
        publish.single("speech", "Juste une seconde, je me renseigne...", hostname=self.mqtt_host)

        if topic_weather_agent is not None and command is not None:
            self.logger.info(f"Send command {command} to agent: {topic_weather_agent}")
        publish.single(topic_weather_agent, command, hostname=self.mqtt_host)
    # ----------------------------------------------------------------------