import os
from abc import ABC, abstractmethod
import logging

class Abstract_Analyzer:

    @property
    @abstractmethod
    def thema(self):
        pass

    def __init__(self):
        print("---- __init__ Abstract_Analyzer")


        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s — %(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s')
        self.logger = logging.getLogger(name = self.__class__.__name__)
        self.initialize()

    @abstractmethod
    def analyze(self, request_is_question: bool, words_list: list) -> None:
        pass

    def initialize(self):
        print("---- initialize Abstract_Analyzer")
        # no need to check, it has been checked in the main program
        self.mqtt_host = os.getenv("MQTT_HOST")
