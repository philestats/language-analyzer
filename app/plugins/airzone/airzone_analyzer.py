import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import paho.mqtt.publish as publish
import Levenshtein
import os
import logging
from ..Abstract_Analyzer import Abstract_Analyzer
from ...lib import common

class Airzone_Analyzer(Abstract_Analyzer):

    @property
    def thema(self):
        return "airzone"

    def __init__(self):
        print("---- __init__ Airzone_Analyzer")
        super().__init__()

    def load_all_topics(self):
        """
        request the mqtt topics on which data from airzone areas are published
        :return: a list of paho MQTTMessages
        """
        topics = subscribe.simple("data/state/ambiance/+/+", hostname=self.mqtt_host, port=self.mqtt_port, msg_count=30, retained=True)
        self.logger.debug(f"+++++ {type(topics)} - {topics}")
        return topics

    def load_rooms_and_measures(self) -> None:
        """
        request all the mqtt topics on which data from airzone areas are published
        :return: a tuple containing the list of the rooms and the list of the measures
        """
        rooms = list()
        measures = list()
        for m in self.load_all_topics():
            #print(m.topic, m.payload)
            topic_tokens = m.topic.split("/")
            rooms.append(topic_tokens[3])
            measures.append(topic_tokens[4].lower())

        self.airzone_rooms = list(set(rooms))
        self.airzone_requests = list(set(measures))

    def get_room_measure(self, room: str, measure: str)-> str:
        # https://stackoverflow.com/questions/57377490/how-to-use-timeout-to-stop-blocking-function-subscribe-simple
        # on chercher dans les topics celui qui correspond au mieux avec room et measure, et on renvoie la valeur
        measure_value = None
        for m in self.load_all_topics():
            self.logger.debug(f"{m.topic} - {m.payload}")
            topic_tokens = m.topic.split("/")
            if Levenshtein.ratio(topic_tokens[3], room)>0.8 and Levenshtein.ratio(topic_tokens[4], measure)>0.8:
                measure_value = m.payload.decode()
                self.logger.debug(f"{measure_value}")
                break
        return measure_value

    def analyze(self, request_is_question: bool, words_list: list) -> None:
        # on cherche si une room de airzone_rooms est présente dans word_list
        self.logger.debug(f"Theme = airzone. Recherche correspondance pièce")
        intersect = common.lists_intersection(words_list, self.airzone_rooms)
        # on suppose qu'on ne passe qu'une piece
        rooms = None
        if len(intersect) > 0:
            rooms = intersect
            self.logger.debug(f"room: {rooms}")

        # on cherche la mesure
        self.logger.debug(f"Theme = airzone. Recherche correspondance mesure")
        intersect = common.lists_intersection(words_list, self.KEYWORDS_MEASURES)
        self.logger.debug(intersect)
        # on suppose qu'on ne demande qu'une measure
        measure = None
        if len(intersect) > 0:
            measure = intersect[0]
            print(f"measure: {measure}")

        # on cherche l'objet
        self.logger.debug(f"Theme = airzone. Recherche correspondance objet")
        intersect = common.lists_intersection(words_list, self.KEYWORDS_OBJECTS)
        print(intersect)
        # on suppose qu'on ne demande qu'un objet
        # pas utilisé
        object = None
        if len(intersect) > 0:
            object = intersect[0]
            print(f"object: {object}")

        # on cherche le verbe
        logging.debug(f"Theme = airzone. Recherche correspondance verbe")
        intersect = common.lists_intersection(words_list, self.KEYWORDS_VERBS)
        print(intersect)
        # on suppose qu'on ne passe qu'un verbe
        # pas utilisé
        verb = None
        if len(intersect) > 0:
            verb = intersect[0]
            print(f"verb: {verb}")

        self.logger.info(f"verb:{verb} object:{object} rooms:{rooms} measures: {measure}")

        room = rooms[0]
        self.logger.info(f"On cherche {measure} dans la piece {room}")
        measure_value = self.get_room_measure(room, measure)
        command = f"La {measure} pour {room} est de {measure_value}  !"
        self.logger.info(f"Send command {command} to agent: speech")
        publish.single(topic="speech", payload=command, hostname=self.mqtt_host)


    def initialize(self):
        self.logger.debug("*** Airzone_Analyzer initialize")
        self.mqtt_host = os.getenv("MQTT_HOST")
        self.mqtt_port = int(os.getenv("MQTT_PORT"))

        self.KEYWORDS_MEASURES = ["température", "humidite", "air_flow", "état"]
        self.KEYWORDS_VERBS = ["régler", "allumer", "éteindre", "arrêter"]
        self.KEYWORDS_OBJECTS = ["clim", "climatisation", "chauffage"]

        self.load_rooms_and_measures()
        self.MATCHWORDS = []
        self.MATCHWORDS.extend(self.airzone_rooms)
        self.MATCHWORDS.extend(self.KEYWORDS_MEASURES)
        self.MATCHWORDS.extend(self.KEYWORDS_VERBS)
        self.MATCHWORDS.extend(self.KEYWORDS_OBJECTS)

        self.KEYWORD_MAPPING =  {"thema": self.thema, "class": __class__, "matchwords": self.MATCHWORDS, "verbs": self.KEYWORDS_VERBS,
          "measures": self.KEYWORDS_MEASURES, "objects": self.KEYWORDS_OBJECTS}
