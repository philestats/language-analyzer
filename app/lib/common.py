import logging
import Levenshtein

QUESTION_SEQUENCES = ["est-ce que", "est-ce qu'il", "va-t-il"]

DAYS = {"Monday": "lundi", "Tuesday": "mardi", "Wednesday": "mercredi", "Thursday": "jeudi", "Friday": "vendredi",
        "Saturday": "samedi", "Sunday": "dimanche"}



def lists_intersection(testing_list: list, reference_list: list)->list:
    # la liste de référence est la 2: elle contient les valeurs de comparaisons fixes
    # l'autre liste contient les valeurs à tester
    testing_set = set(testing_list)
    reference_set = set(reference_list)
    logging.debug(f"   Set 1: {testing_set}")
    logging.debug(f"   Set 2: {reference_set}")
    #intersect1 = list(testing_set.intersection(reference_set))
    intersect = list()
    for item1 in testing_set:
        for item2 in reference_set:
            similarity_ratio = Levenshtein.ratio(item1, item2)
            #print(f"Comparaison {item1} et {item2}: ratio: {similarity_ratio}")
            if similarity_ratio > 0.7:
                intersect.append(item2)
            #if item1.lower() == item2.lower():
            #    intersect2.append(item2)

    logging.debug(f"intersect = {intersect}")
    return intersect
# ----------------------------------------------------------------------