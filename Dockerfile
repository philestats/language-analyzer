FROM balenalib/armv7hf-debian

RUN [ "cross-build-start" ]


# set the working directory in the container
WORKDIR /code


RUN apt-get update && \
    apt install --no-install-recommends -y \
    python3.9-minimal python3-pip cmake ninja-build \
    build-essential libssl-dev python3-dev python3-numpy

# copy the dependencies file to the working directory
COPY requirements.txt /
COPY pip.conf /etc/

RUN pip install --upgrade pip setuptools wheel
RUN pip install --no-cache-dir --prefer-binary -r /requirements.txt



# copy the content of the local src directory to the working directory
COPY app/ /app/

# command to run on container start
CMD [ "python3", "/app/language_analyzer.py" ]

RUN [ "cross-build-end" ]

# ref. https://community.home-assistant.io/t/migration-to-2021-7-fails-fatal-python-error-init-interp-main-cant-initialize-time/320648/10
